Simple Calculator

Build a simple calculator using Vue.js 3. The calculator should allow the user to perform basic arithmetic operations such as addition, subtraction, multiplication, and division.
Requirements

    The calculator should have a display area that shows the current calculation or result.
    The calculator should have buttons for each of the numbers from 0-9.
    The calculator should have buttons for each of basic arithmetic operations.
    The calculator should have a button for clearing the display area
    Users should be able to input numbers and decimal points using the calculator's buttons.
    Users should be able to perform basic arithmetic operations using the calculator's buttons.
    Users should be able to clear the calculator's display and reset the calculation.

Bonus

    Implement keyboard support so that users can input numbers and perform operations using their keyboard.
